discourse-sequence-diagram
=======================

Sequence diagram plugin for [Discourse](http://discourse.org)

This is just an embedded call to the great [js-sequence-diagrams library](https://bramp.github.io/js-sequence-diagrams)
