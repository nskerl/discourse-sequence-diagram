(function() {
	function insertDiagram(_, contents) {
		return "<div class='discourse-sequence-diagram'>" + contents + "</div>";
	}

	function replaceDiagrams(text) {
		text = text || "";
		text = text.replace(/\[seqdiagram\]((?:.|\r?\n)*?)\[\/seqdiagram\]/igm, insertDiagram);
		return text;
	}
  
	Discourse.Dialect.addPreProcessor(function(text) {
		if (Discourse.SiteSettings.enable_sequence_diagram_plugin) {
		  text = replaceDiagrams(text);
		}
		return text;
	});
	Discourse.Markdown.whiteListTag('div', 'class', /^discourse-sequence-diagram$/igm);
})();
