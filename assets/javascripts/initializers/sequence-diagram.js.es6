import { withPluginApi, decorateCooked } from 'discourse/lib/plugin-api';

function convertToDiagram($elem) {
	$elem.find("div.discourse-sequence-diagram").sequenceDiagram({theme: 'simple'});
}

export default {
	name: 'discourse-sequence-diagram',
	initialize(container) {	
		const siteSettings = container.lookup('site-settings:main');
		if (siteSettings.enable_sequence_diagram_plugin) {
			withPluginApi('0.1', api => {
				api.decorateCooked(convertToDiagram);
			});
		}
	}
}
