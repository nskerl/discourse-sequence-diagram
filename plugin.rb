# name: Sequence Diagram
# about: Draw a sequence diagram
# version: 0.1
# authors: Nathan Skerl

register_asset "javascripts/sequence_diagram_dialect.js", :server_side
register_asset "javascripts/raphael-min.js"
register_asset "javascripts/sequence-diagram-min.js"